import numpy as np
import scipy.sparse.csgraph
from itertools import product

# part 0 : parsing

data = np.genfromtxt(
    "input",
    delimiter=1
)

num_nodes = data.shape[0] * data.shape[1]

def neighbor_coords(xy):
    """ return bool matrix marking neighbors of coords `xy`
        yes I took this from my day11
    """
    assert xy.shape == (2,), xy.shape

    # add offsets to input coord
    offsets = np.array([
                 [0,-1],         
        [-1, 0],         [1, 0],
                 [0, 1],
    ])
    offxys = xy + offsets
    assert offxys.shape == (4,2), offxys.shape

    # keep only the in-bound coords
    return offxys[
        (0 <= offxys[:,0]) & (offxys[:,0] < data.shape[0]) &
        (0 <= offxys[:,1]) & (offxys[:,1] < data.shape[1])
    ]


# make N x N distance matrix
distances = np.zeros((num_nodes, num_nodes))
input_coords = np.array([
    [x, y]
    for x, y in product(range(data.shape[0]), range(data.shape[1]))
])
for xy in input_coords:
    x, y = xy
    di = (y * data.shape[1]) + x
    assert di < num_nodes

    for nx, ny in neighbor_coords(xy):
        ni = (ny * data.shape[1])  + nx
        assert ni < num_nodes
        distances[di, ni] = data[y,x]
        distances[ni, di] = data[ny,nx]

path = scipy.sparse.csgraph.shortest_path(distances)
path_distance = int(path[-1,0])
print(f"Path 1: {path_distance}")
