import numpy as np

# part 1
depths = np.loadtxt("input")
diffs = np.subtract(depths[1:], depths[:-1])
increasing = diffs[diffs > 0]
print(f"Part 1: {len(increasing)}")

# part 2
windows = np.lib.stride_tricks.sliding_window_view(depths, 3)
windowsums = np.sum(windows, axis=1)
diffs = np.subtract(windowsums[1:], windowsums[:-1])
increasing = diffs[diffs > 0]
print(f"Part 2: {len(increasing)}")
