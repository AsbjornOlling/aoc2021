with open("input") as f:
  text = f.read().strip()

depths = [int(l.strip()) for l in text.split("\n")]

# part 1
def count_increasing(xs):
    return len([
        diff
        for a, b in zip(xs[1:], xs[:-1])
        if (diff := a - b) > 0
    ])

# part 1
print(f"Part 1: {count_increasing(depths)}")

# part 2
size = 3
windows = [sum(depths[i:i+size]) for i in range(len(depths))]
print(f"Part 2: {count_increasing(windows)}")
