import System.IO  
import Control.Monad

main = do
  -- read input
  filetxt <- readFile "testinput"
  let d = (map read $ lines filetxt) :: [Integer]

  -- part 1
  let diff = zipWith (-) (tail d) (init d)
  let positives = filter (\x -> x > 0) diff
  print $ length positives

  -- part 2
  let d2 = zipWith (-) (drop 3 d) d
  print $ length $ filter ((<) 0) d2


f :: (a -> b) -> [a] -> [b]
f = map
