import numpy as np

# day 4

inputfile = "input"

# part 0: parsing
nums = np.genfromtxt(
  inputfile,
  delimiter=",",
  max_rows=1,
  autostrip=True,
  dtype=float,  # needs to be float so we can nan
)

boardsize = 5  # assumes square boatds
boards = np.genfromtxt(
    inputfile,
    dtype=float,  # needs to be float so we can nan
    skip_header=1
).reshape((-1,5,5))


# part 1: detect bingo winner

# make matrix where the rows are mentioned number at each "step"
# e.g. [1, nan, nan];
#      [1, 2,   nan];
#      [1, 2,   3  ];
num_seqs = np.tile(nums, (len(nums), 1))
assert num_seqs.shape == (len(nums), len(nums))
num_seqs[np.triu_indices(num_seqs.shape[0], k=1)] = np.nan


def has_won(board):
    assert board.shape == (5, 5)
    b = np.logical_not(np.isnan(board))
    rowwin = np.any(np.all(b, axis=0))
    colwin = np.any(np.all(b, axis=1))
    return rowwin or colwin


def winners(seq):
    """ given an array of mentioned numbers,
        return an array indicating which boards have won
    """ 
    said_nums = set(seq[~np.isnan(seq)])
    has_been_said = np.vectorize(lambda x: x in said_nums)

    # hmm I can probably get rid of this list comprehension
    # but I need to move on
    return np.array([
        has_won(np.where(
            has_been_said(b),
            b,
            np.zeros(b.shape) * np.nan
        ))
        for b in boards
    ])


# label winners on each turn
winstates = np.apply_along_axis(winners, 1, num_seqs)

# find the first turn with a winner
winturn = np.argmax(np.any(winstates, axis=1))

# the sequence said up to that point
winturn_seq = num_seqs[winturn][~np.isnan(num_seqs[winturn])]

# the winning number of that turn
winturn_num = winturn_seq[-1]

# the board that wins that turn
winboard = boards[np.argmax(winstates[winturn])]

# unmarked numbers on that board
unmarked = winboard[np.vectorize(lambda x: x not in winturn_seq)(winboard)]

print(f"Part 1: {sum(unmarked) * winturn_num}")


# part 2, almost the same thing

# now find the turn where the last board wins
lastwinturn = np.argmax(np.all(winstates, axis=1))
lastwinturn_seq = num_seqs[lastwinturn][~np.isnan(num_seqs[lastwinturn])]
lastwinturn_num = lastwinturn_seq[-1]
lastwinboard = boards[np.argmin(winstates[lastwinturn-1])]
lastunmarked = lastwinboard[np.vectorize(lambda x: x not in lastwinturn_seq)(lastwinboard)]

print(f"Part 2: {sum(lastunmarked) * lastwinturn_num}")
