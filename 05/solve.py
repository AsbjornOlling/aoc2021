import io
import numpy as np

# part 0: parse

with open("input") as f:
    rawtext = f.read().strip()

lines = np.genfromtxt(
    io.StringIO(rawtext.replace(" -> ", ",")),
    delimiter=",",
    dtype=np.int16
)


# part 1

# filter for horizontal and vertical lines
nondiagonal_idxs = np.logical_or(
    lines[:,0] == lines[:,2],
    lines[:,1] == lines[:,3]
)
nondiagonal = lines[nondiagonal_idxs]

# make empty map
xmax = np.max(lines[:,[0,2]]) + 1
ymax = np.max(lines[:,[1,3]]) + 1
themap = np.zeros((ymax,xmax))


def line_coords(line):
    """ blit the line onto a map """
    steps = np.max(np.abs(line[[0,1]] - line[[2,3]])) + 1
    xs = np.linspace(line[0], line[2], steps, dtype=np.int16)
    ys = np.linspace(line[1], line[3], steps, dtype=np.int16)
    coords = np.stack([ys, xs], axis=0)

    m = themap.copy()
    m[tuple(coords)] = 1
    return m


# stack all the maps
map_layers = np.apply_along_axis(line_coords, 1, nondiagonal)
flatmap = np.sum(map_layers, axis=0)

# count intersections
result = np.count_nonzero(flatmap >= 2)
print(f"Part 1: {result}")


# part 2

# do it again, but for all lines
map_layers = np.apply_along_axis(line_coords, 1, lines)
flatmap = np.sum(map_layers, axis=0)
result = np.count_nonzero(flatmap >= 2)
print(f"Part 2: {result}")
