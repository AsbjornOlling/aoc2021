import numpy as np

# day 02

# part 0 - parsing
str_input = np.loadtxt("input", dtype=str)

def instruction_column(sym):
    """ Column with all instructions of type `sym` """
    return (str_input[:,0] == sym) * str_input[:,1].astype(np.int64)

forwards = instruction_column("forward")
verticals = instruction_column("down") - instruction_column("up")

# part 01 - position is just sum of moves...
result = sum(forwards) * sum(verticals)
print(f"Part 1: {result}")

# part 02
# do accumulating sum of ups and downs to find "aim",
# then multiply with forwards to find depth changes
result = sum(forwards) * sum(forwards * np.cumsum(verticals))
print(f"Part 2: {result}")
