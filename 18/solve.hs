import Text.ParserCombinators.ReadP
import Data.Char
import Data.Maybe
import Debug.Trace

data Tree a = Pair (Tree a) (Tree a)
            | Leaf a
            deriving Eq

type SnailNumber = Tree Int

instance (Show a) => Show (Tree a) where
  show = showTree

showTree :: Show a => (Tree a) -> String
showTree (Pair left right) = "[" ++ (showTree left) ++ "," ++ (showTree right) ++ "]"
showTree (Leaf x) = show x


{- Part 0: Parsing -}

snailLiteral :: ReadP SnailNumber
snailLiteral = do
  numStr <- many1 $ satisfy isDigit
  return $ Leaf (read numStr)


snailNested :: ReadP SnailNumber
snailNested = do
  char '['
  a <- snailNumber
  char ','
  b <- snailNumber
  char ']'
  return $ Pair a b


snailNumber :: ReadP SnailNumber
snailNumber = do
  snailLiteral <++ snailNested


snailNumberLines :: ReadP [SnailNumber]
snailNumberLines =
  let
    numberLine = do
      snum <- snailNumber
      char '\n'
      return snum
  in
    many1 numberLine


parseSnailNumbers :: String -> Maybe [SnailNumber]
parseSnailNumbers str =
  let res = reverse (readP_to_S snailNumberLines $ str) in
  case res of
    (sn, ""):_ -> Just sn
    _          -> Nothing


parseSnailNumber :: String -> Maybe SnailNumber
parseSnailNumber str =
  case reverse (readP_to_S snailNumber $ str) of
    [(sn, "")] -> Just sn
    _          -> Nothing


{- Part 1: Addition -}

split :: SnailNumber -> SnailNumber
split = fst . doSplit


doSplit :: SnailNumber -> ( SnailNumber, Bool )
doSplit (Pair a b) =
  let (newA, splitA) = doSplit a in
  if splitA then
    (Pair newA b, splitA)
  else
    let (newB, splitB) = doSplit b in
    (Pair newA newB, splitB)
doSplit (Leaf a) =
  if a < 10 then
    (Leaf a, False)
  else
    let half = (fromIntegral a) / 2 in
    ( Pair
        (Leaf $ floor half)
        (Leaf $ ceiling half)
    , True
    )


{- Explosion -}

explode :: SnailNumber -> SnailNumber
explode snum =
    toSnailNum $ explosionTree snum


data ExplosionMark = Unmarked Int
                   | NearestLeft Int
                   | Explode Int
                   | NearestRight Int
                   deriving Show

type ExplosionTree = Tree ExplosionMark


setRight :: c -> ( a, b, c ) -> ( a, b, c )
setRight c ( a, b, _ ) = ( a, b, c )


setLeft :: a -> ( a, b, c ) -> ( a, b, c )
setLeft a ( _, b, c ) = ( a, b, c )


explosionTree :: SnailNumber -> ExplosionTree
explosionTree = fst . buildExplosionTree 0


buildExplosionTree :: Int -> SnailNumber -> (ExplosionTree, ( Bool, Maybe ( Int, Int ), Bool ))
buildExplosionTree _ (Leaf x) = (Leaf $ Unmarked x, ( False, Nothing, False ))
buildExplosionTree 4 (Pair (Leaf a) (Leaf b)) =
  -- exploding pair found
  ( Pair
      (Leaf $ Explode a)
      (Leaf $ Explode b)
  , (False, Just (a, b), False)
  )
buildExplosionTree depth (Pair a b) =
  -- DFS and mark one pair with depth > 4
  -- return True if explosion pair has been found
  -- XXX: only marks Explode rn
  let ( markedA, statusA ) = buildExplosionTree (depth + 1) a
  in case statusA of
      -- nearest right found, just return
      ( _, Just _, True ) -> ( Pair markedA $ allUnmarked b, statusA )
      -- still need to find right
      ( _, Just (_, n), False ) ->
        let rightTree = addToRight n b
        in ( Pair markedA rightTree, setRight True statusA )
      -- didn't find explosion
      ( _, Nothing, _ ) ->
        let ( markedB, statusB ) = buildExplosionTree (depth + 1) b in
        case statusB of 
          -- nearest left found, just return
          ( True, Just _, _ )  -> ( Pair (allUnmarked a) markedB, statusB )
          -- still need to find left
          ( False, Just (n, _), _ ) ->
            let leftTree = addToLeft n a
            in ( Pair leftTree markedB, setLeft True statusB )
          -- nothing found
          ( _, Nothing, _ )    -> ( Pair markedA markedB, statusB )


-- mark rightmost node in tree as `NearestLeft`, add `n` to value
addToLeft :: Int -> SnailNumber -> ExplosionTree
addToLeft n (Pair a (Leaf b)) = Pair (allUnmarked a) (Leaf $ NearestLeft (b + n))
addToLeft n (Leaf x)          = Leaf $ NearestLeft (x + n)
addToLeft n (Pair a b)        = Pair (allUnmarked a) (addToLeft n b)

-- mark leftmost node in tree as `NearestRight`
addToRight :: Int -> SnailNumber -> ExplosionTree
addToRight n (Pair (Leaf a) b) = Pair (Leaf $ NearestRight $ n + a) (allUnmarked b)
addToRight n (Leaf x)          = Leaf $ NearestRight $ n + x
addToRight n (Pair a b)        = Pair (addToRight n a) $ allUnmarked b

-- mark all leaves as `Unmarked`
allUnmarked :: SnailNumber -> ExplosionTree
allUnmarked (Pair a b) = Pair (allUnmarked a) (allUnmarked b)
allUnmarked (Leaf x) = Leaf $ Unmarked x

-- marked explosion tree back to snail number
toSnailNum :: ExplosionTree -> SnailNumber
toSnailNum (Pair x (Pair (Leaf (Explode a)) (Leaf (Explode b)))) = Pair (toSnailNum x) (Leaf 0)
toSnailNum (Pair (Pair (Leaf (Explode a)) (Leaf (Explode b))) x) = Pair (Leaf 0) (toSnailNum x)
toSnailNum (Pair a b) = Pair (toSnailNum a) (toSnailNum b)
toSnailNum (Leaf (Unmarked x)) = Leaf x
toSnailNum (Leaf (NearestLeft x)) = Leaf x
toSnailNum (Leaf (NearestRight x)) = Leaf x


reduce :: SnailNumber -> SnailNumber
reduce snum =
  -- If any pair is nested inside four pairs, the leftmost such pair explodes.
  -- If any regular number is 10 or greater, the leftmost such regular number splits.
  let 
    -- exploded = explode $ trace (show snum) snum
    exploded = explode snum
    newSnum =
      if exploded == snum then
        split snum
      else
        exploded
  in
    if newSnum == snum then
      -- fixed point reached. return
      newSnum
    else
      reduce newSnum


add :: SnailNumber -> SnailNumber -> SnailNumber
add a b =
  let
    newpair = Pair a b
    result = reduce newpair
  in
    trace
      ("  " ++ show a ++ "\n+ " ++ show b ++ "\n= " ++ show result)
      result


magnitude :: SnailNumber -> Int
magnitude (Pair a b) = (3 * magnitude a) + (2 * magnitude b)
magnitude (Leaf x) = x


-- part 2:

part2 :: [SnailNumber] -> Int
part2 snums =
  let 
    sumMagnitudes :: [Int]
    sumMagnitudes = [magnitude $ add a b | a <- snums, b <- snums]
  in
    maximum sumMagnitudes
    
  


main :: IO ()
main = do
  filetxt <- readFile "input"
  let snums = parseSnailNumbers filetxt

  putStr "Initial: "
  print $ snums

  -- part 1:
  let reduced = fmap (\sns -> foldl add (head sns) $ tail sns) snums

  putStr "Reduced: "
  print $ reduced

  putStr "Magnitude: "
  print $ fmap magnitude reduced

  -- part 2:
  print $ fmap part2 snums
