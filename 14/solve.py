import numpy as np
from functools import partial

# part 0 : parsing

inputfile = "input"
rules = np.genfromtxt(
    inputfile,
    skip_header=1,
    delimiter=" -> ",
    dtype=str
)
template = np.genfromtxt(
    inputfile,
    delimiter=1,
    skip_footer=len(rules),
    dtype=str
)

rule_pairs = np.array([list(s) for s in rules[:,0].flatten()], dtype=str)
rule_insertions = rules[:,1]
rules = np.hstack([rule_pairs, rule_insertions.reshape((-1,1))])

# part 1 : do 10 steps

def insertion_char(pair):
    """ insertion char for a pair, can be empty string """
    assert pair.shape == (2,), pair.shape
    matches = np.apply_along_axis(partial(np.array_equal, pair), 1, rule_pairs)
    if not (s := np.sum(matches)):
        return ""
    assert s == 1
    return rule_insertions[np.argmax(matches)]


def step_polymer(polymer):
    """ do one insertion step on polymer """
    # pairs of neighbors in list
    zipped = np.stack([polymer[:-1], polymer[1:]]).transpose()

    # insertion values if zipped pair is in rules
    inserts = np.apply_along_axis(insertion_char, 1, zipped)
    inserts = np.append(inserts, [""])  # pad to length
    assert inserts.shape == polymer.shape

    # interleave lists,
    new_polymer = np.ravel((polymer, inserts), order="F")

    # drop empty and return
    return new_polymer[new_polymer != ""]


polymer = template
for _ in range(10):
    polymer = step_polymer(polymer)

element_count = np.bincount(np.vectorize(ord)(polymer))

answer = np.max(element_count) - np.min(element_count[element_count > 0])
print(f"Part 1: {answer}")
