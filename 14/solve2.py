import numpy as np
from collections import Counter
import string

# part 0: parsing

inputfile = "input"
rules = np.genfromtxt(
    inputfile,
    skip_header=1,
    delimiter=" -> ",
    dtype=str
)
polymer = np.genfromtxt(
    inputfile,
    delimiter=1,
    skip_footer=len(rules),
    dtype=str
)

# rule_pairs = np.array([list(s) for s in rules[:,0].flatten()], dtype=str)
# rules = np.hstack([rule_pairs, rule_insertions.reshape((-1,1))])
rule_insertions = rules[:,1]

# enumeration for pairs
iof = {p:i for i, p in enumerate(rules[:,0])}
fromi = {i:p for p, i in iof.items()}

replacement_rule = {
    iof[parents]: np.array([iof[f"{parents[0]}{new}"], iof[f"{new}{parents[1]}"]])
    for parents, new in rules
}


# find initial pairs
initial_pairs = np.stack([polymer[:-1], polymer[1:]]).transpose()
initial_pairs = np.apply_along_axis(lambda p: f"{p[0]}{p[1]}", 1, initial_pairs)
initial_pair_idxs = np.vectorize(iof.get)(initial_pairs)

# populate count
pair_count = np.zeros(len(rules))
for p in initial_pair_idxs:
    pair_count[p] += 1

for _ in range(40):
    # find non-zero pairs
    pair_idxs = np.where(pair_count != 0)[0]
    assert pair_idxs.ndim == 1

    # new pairs from dict lookup
    next_pairs = np.array([
        (old, new)
        for old in pair_idxs
        for new in replacement_rule.get(old, [old])
    ])
    print(f"next_pairs={[fromi[i] for i in next_pairs[:,1]]}")

    # make new pair count
    new_pair_count = np.zeros(len(pair_count))
    for old, new in next_pairs:
        new_pair_count[new] += pair_count[old]

    pair_count = new_pair_count


c: Counter = Counter()
c[polymer[0]] += 1
c[polymer[-1]] += 1
for i, count in enumerate(pair_count):
    c[fromi[i][0]] += count
    c[fromi[i][1]] += count

charcounts = np.array(list(c.values()))

answer = (np.max(charcounts)/2) - (np.min(charcounts)/2)
print(f"Part 2: {answer}")
