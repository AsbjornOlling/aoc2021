import numpy as np

# part 0: partsing

data = np.genfromtxt(
    "input",
    delimiter=",",
    dtype=np.int16
)


# part 1 - lazy brute-y solution

# range of possible horizontal pos
possibles = np.arange(np.min(data), np.max(data))

# matrix shaping so we can do one subtract to find all distances
possibles_m = np.tile(possibles, (len(data), 1)).transpose()
crabs_m = np.tile(data, (possibles_m.shape[0], 1))

# sum and find the smallest one
all_fuel_spent = np.abs(crabs_m - possibles_m)
total_fuel_spent = np.sum(all_fuel_spent, axis=1)
assert total_fuel_spent.ndim == 1
smallest_distance = np.min(total_fuel_spent)

print(f"Part 1: {smallest_distance}")


# part 2, new formula for fuel cost

# make the fun
fuel_cost = np.vectorize(lambda x: (x*(x+1)/2))

# apply new fuel cost function to fuel spent matrix
new_all_fuel_spent = fuel_cost(all_fuel_spent)
new_total_fuel_spent = np.sum(new_all_fuel_spent, axis=1)

# find new optimum
new_smallest_distance = np.min(new_total_fuel_spent)

print(f"Part 2: {int(new_smallest_distance)}")
