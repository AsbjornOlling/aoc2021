import numpy as np


# part 0: parsing 

with open("input") as f:
    txt = f.read()

rows = np.array([str(l) for l in txt.strip().split("\n")], dtype=str)


# part 1: not very numpy-y - I tried...

end_start = {c: chr(ord(c)-2) for c in "]}>"} | {")":"("}
part1_points = {")": 3, "]": 57, "}": 1197, ">": 25137}
part2_points = {"(": 1, "[": 2,  "{": 3,    "<": 4}

def corruption_points(row):
    stack: list[int] = []
    for c in row:
        if c in end_start.values():
            # correct open
            stack.append(c)
        elif stack and end_start.get(c) == stack[-1]:
            # correct close
            stack.pop()
        else:
            # incorrect close, part1 points
            return part1_points[c]

    # part 2 points
    points = np.vectorize(part2_points.get)(stack[::-1])
    mult = 5 ** np.arange(len(stack))[::-1]  # yass weird algorithm vectorized
    score = np.sum(points * mult)

    # make these negative, so we can separate them from part1 points
    return -1 * score


points = np.vectorize(corruption_points)(rows)
print(f"Part 1: {np.sum(points[points > 0])}")


# part 2
answer = int(np.median(points[points < 0])) * -1
print(f"Part 2: {answer}")
