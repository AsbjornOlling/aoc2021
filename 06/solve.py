import numpy as np

# part 0: parsing

data = np.genfromtxt("input", delimiter=",", dtype=np.uint8)

# part 1

# vector w/ ones in the positions where fish are added at birth
# useful to multiply with num of birthing fish, to get new fish count
birthmask = np.zeros(9, dtype=np.uint8)
birthmask[6] = 1

# function to do one breed step
# decrement by shifting left, then add new fish using birthmask
breed_step = lambda a, _: (np.roll(a, -1)) + (birthmask * a[0])
breed_step = np.frompyfunc(breed_step, 2, 1)

# do `days` breed steps
def breed_ndays(days):
    return int(np.sum(breed_step.reduce(
        np.zeros(days),
        initial=np.bincount(data, minlength=9)
    )))

# 🌟
print(f"Part 1: {breed_ndays(80)}")

# part 2
print(f"Part 2: {breed_ndays(256)}")
