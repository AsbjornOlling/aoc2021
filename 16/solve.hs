import Text.ParserCombinators.ReadP
import Numeric (showIntAtBase)
import Data.Char (intToDigit)
import Data.List
import Numeric (readHex)
import Debug.Trace


{- Packet and operation types -}

type Version = Int
data Operation = Sum         -- 0
               | Product     -- 1
               | Minimum     -- 2
               | Maximum     -- 3
               | GreaterThan -- 5
               | LessThan    -- 6
               | Equal       -- 7

instance Show Operation where
  show op =
    case op of
      Sum         -> "Sum"
      Product     -> "Product"
      Minimum     -> "Minimum"
      Maximum     -> "Maximum"
      GreaterThan -> "GreaterThan"
      LessThan    -> "LessThan"
      Equal       -> "Equal"

opFromNum :: Int -> Operation
opFromNum n =
  case n of
    0 -> Sum
    1 -> Product
    2 -> Minimum
    3 -> Maximum
    5 -> GreaterThan
    6 -> LessThan
    7 -> Equal


data Packet = NestedPacket Version Operation [Packet]
            | LiteralPacket Version Int

instance Show Packet where
  show (NestedPacket version op children) =
    let
      childstr :: String
      childstr = intercalate "\n" $ map show children
    in
      "NestedPacket | Version: " ++ show version ++ " Op: " ++ show op ++ "\n" ++ childstr

  show (LiteralPacket version num) =
    "LiteralPacket | Version: " ++ show version ++ " Num: " ++ show num


{- Part 2 solve: recursive evaluate -}
evaluate :: Packet -> Int
evaluate (LiteralPacket _ num) = num
evaluate (NestedPacket _ op children) =
  let
    f =
      case op of
        Sum -> sum
        Product -> product
        Minimum -> minimum
        Maximum -> maximum
        GreaterThan -> (\(a:b:_) -> if a>b then 1 else 0)
        LessThan ->    (\(a:b:_) -> if a<b then 1 else 0)
        Equal ->       (\(a:b:_) -> if a==b then 1 else 0)
    in f $ map evaluate children

{- Part 1 solve: recursive sum -}
versionSum :: Packet -> Int
versionSum (LiteralPacket version _) = version
versionSum (NestedPacket version _ children) =
  version + sum (map versionSum children)


{- Numbery Utils -}

toBinaryStr :: Int -> String
toBinaryStr n =
  showIntAtBase 2 intToDigit n ""

fromBinaryStr :: String -> Int
fromBinaryStr [] = 0
fromBinaryStr (bit:bits) =
  (if bit == '1' then (2^length bits) else 0) + fromBinaryStr bits

hexNibble :: Char -> String
hexNibble hexc =
  -- yeah this isn't great
  -- but it's not *that* verbose
  -- and it's very readible
  case hexc of
    '0' -> "0000"
    '1' -> "0001"
    '2' -> "0010"
    '3' -> "0011"
    '4' -> "0100"
    '5' -> "0101"
    '6' -> "0110"
    '7' -> "0111"
    '8' -> "1000"
    '9' -> "1001"
    'A' -> "1010"
    'B' -> "1011"
    'C' -> "1100"
    'D' -> "1101"
    'E' -> "1110"
    'F' -> "1111"

parseHexPacket :: String -> Maybe Packet
parseHexPacket hexStr =
  -- ^ Top-level packet parser from hex
  let
    parser = (readP_to_S packet) :: ReadS Packet
    bitStr = (concat $ map hexNibble hexStr) :: String
  in
    case parser bitStr of
      [(p, _)] -> Just p
      _ -> Nothing

packet :: ReadP Packet
packet =
  -- ^ binary packet parser
  literalPacket <++ nestedPacket

nestedPacket :: ReadP Packet
nestedPacket = do
  version <- threeBits
  opNum <- threeBits
  ltype <- get
  children <- case ltype of '0' -> ltypeZero
                            '1' -> ltypeOne
                            _   -> pfail
  return $ NestedPacket version (opFromNum opNum) children

ltypeZero :: ReadP [Packet]
ltypeZero = do
  packetLenBin <- count 15 get
  packetBits <- count (fromBinaryStr packetLenBin) get
  case reverse $ readP_to_S (many1 packet) packetBits of
    (ps,""):_ -> return ps
    _ -> pfail

ltypeOne :: ReadP [Packet]
ltypeOne = do
  elevenBits <- count 11 get
  count (fromBinaryStr elevenBits) packet

literalPacket :: ReadP Packet
literalPacket = do
  version <- threeBits
  string "100" -- packet type 0x04
  chunks <- many chunk
  last <- lastChunk
  let num = fromBinaryStr $ (concat chunks) ++ last
  return $ LiteralPacket version num

chunk :: ReadP String
chunk = do
  char '1'
  bitstr <- count 4 get
  return bitstr

lastChunk :: ReadP String
lastChunk = do
  char '0'
  bitstr <- count 4 get
  return bitstr

threeBits :: ReadP Int
threeBits = do
  bitstr <- count 3 get 
  return $ fromBinaryStr bitstr


main :: IO ()
main = do
  filetxt <- readFile "input"
  let parsed = parseHexPacket filetxt

  putStr "Part 1: "
  let sum = fmap versionSum parsed :: Maybe Int
  print $ sum

  putStr "Part 2: "
  let result = fmap evaluate parsed :: Maybe Int
  print $ result
