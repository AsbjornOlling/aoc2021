import numpy as np
import networkx as nx
from tqdm import tqdm
from multiprocessing.dummy import Pool

# part 0: parsing

data = np.genfromtxt("testinput1", delimiter="-", dtype=str)
assert data.shape[1] == 2

# part 1

# N is the amount of times a path may visit a large cave
N = 6

# pick out large caves
is_upper = np.vectorize(lambda s: s.isupper())
large_caves_only = data[is_upper(data[:,0]) | is_upper(data[:,1])]
assert large_caves_only.shape[1] == 2

# sort edges so large caves are always on the left
large_caves_left = np.apply_along_axis(
    lambda edge: edge[::-1] if edge[1].isupper() else edge,
    1,
    large_caves_only
)

# make N edges for each edge that includes large caves
extra_edges = np.apply_along_axis(
    lambda edge: np.array([[f"{edge[0]}:{n:02d}", edge[1]] for n in range(N)]),
    1,
    large_caves_left
).reshape((-1,2))
assert extra_edges.shape == (large_caves_only.shape[0] * N, 2)

all_edges = np.concatenate([data, extra_edges])
assert all_edges.shape[1] == 2

# build networkx graph for part 1

def all_paths(edges):
    graph = nx.Graph() 
    graph.add_edges_from(edges)

    # find all simple paths with nx
    lpaths = list(nx.all_simple_paths(graph, "start", "end"))

    # restore original cave names
    paths = np.array([
        ",".join(c.split(":")[0] for c in path)
        for path in lpaths
    ])

    # eliminate duplicate paths
    uniques = np.unique(paths, axis=0)
    return uniques


# part1_paths = all_paths(all_edges)
# # ⭐
# print(f"Part 1: {all_edges}")


# part 2

# loop over small caves
is_small = np.vectorize(lambda x: x.islower() and x not in ("start", "end"))
only_small_caves = all_edges[is_small(all_edges[:,0]) | is_small(all_edges[:,1]),:]
small_caves = np.unique(only_small_caves[is_small(only_small_caves)])

def paths_visit_twice(scave):
    """ Get all paths for graph, where scave can be visited twice """
    # filter for edges that include scave
    scave_edges = only_small_caves[(only_small_caves[:,0] == scave) | (only_small_caves[:,1] == scave),:]

    # sort edges so scave is always in column 0
    scave_left = np.apply_along_axis(
        lambda edge: edge if edge[0] == scave else edge[::-1],
        1,
        scave_edges
    )

    # rename scave edge to avoid collision
    # these are the edges that simulate allowing two visits
    extra_smallcave_edges = np.apply_along_axis(
        lambda edge: np.array([f"{edge[0]}:{1:02d}", edge[1]]),
        1,
        scave_left
    )

    # make graph with one more small cave edge
    sim_edges = np.concatenate([all_edges, extra_smallcave_edges])
    paths = all_paths(sim_edges)

    return paths


# all_paths = np.concatenate([paths_visit_twice(scave) for scave in tqdm(small_caves)])

p = Pool(ps := len(small_caves))
print(f"Making {ps} processes...")
all_paths = np.concatenate(p.map(paths_visit_twice, small_caves))

unique_paths = np.unique(all_paths)

answer = len(unique_paths)
print(f"Part 2: {answer}")
