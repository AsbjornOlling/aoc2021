import numpy as np

# part 0: parsing

data = np.genfromtxt("input", dtype=str)

def str_to_vec(s):
    return np.array([c in s for c in "abcdefg"], dtype=bool)

assert np.all(str_to_vec("abcdefg") == np.ones(7).astype(bool))
assert np.all(str_to_vec("") == np.zeros(7).astype(bool))
assert np.all(str_to_vec("ga") == np.array([1,0,0,0,0,0,1]).astype(bool))

def strs_to_vec(ss):
    return np.array([str_to_vec(s) for s in ss])

unique_symbols = np.apply_along_axis(strs_to_vec, 1, data[:,:10])
output_numbers = np.apply_along_axis(strs_to_vec, 1, data[:,11:])
assert unique_symbols.shape[2] == 7
assert output_numbers.shape[2] == 7
print(output_numbers[0])

# part 1: count 1s, 4s, 7s, and 8s
segment_counts = np.sum(output_numbers, axis=2)
ones = segment_counts == 2
fours = segment_counts == 4
sevens = segment_counts == 3
eights = segment_counts == 7
answer = np.sum(ones + fours + sevens + eights)
print(f"Part 1: {answer}")


# part 2: decode all outputs
# haha fuck me this is ugly

def row_to_num(row):
    """ treat row as binary number """
    return np.sum((row*2) ** np.arange(7, 0, -1))

assert row_to_num(np.array([False, False, False, True, True, False, True])) == 26


def deduce_symbols(symbols_row):
    """ make an array of all ten symbols, ordered """
    segment_counts = np.sum(symbols_row, axis=1)

    def nums_by_segcount(segcount):
        return symbols_row[segment_counts == segcount]

    # we know these ones already
    one = nums_by_segcount(2)
    assert one.shape == (1, 7)
    one = one[0]

    four = nums_by_segcount(4)
    assert four.shape == (1, 7)
    four = four[0]

    seven = nums_by_segcount(3)
    assert seven.shape == (1, 7)
    seven = seven[0]

    eight = nums_by_segcount(7)
    assert eight.shape == (1, 7)
    eight = eight[0]

    # top index is the only segemnt that is in seven, but not in one
    top_idx = np.argmax(one ^ seven)

    # these are zero, six or nine
    zero_six_nine = nums_by_segcount(6)
    assert zero_six_nine.shape == (3,7)

    # six is the one that shares only one "on" segment with one
    zsn_onesegs = zero_six_nine[:,one]
    assert zsn_onesegs.shape == (3,2)
    six = zero_six_nine[zsn_onesegs[:,0] ^ zsn_onesegs[:,1]]
    assert six.shape == (1,7)
    six = six[0]

    # the other two are zero or nine
    zero_nine = zero_six_nine[zsn_onesegs[:,0] & zsn_onesegs[:,1]]
    assert zero_nine.shape == (2,7)

    # nine is the one that has all of 4's segments on
    all_fours_on = np.all(zero_nine[:,four], axis=1)
    nine = zero_nine[all_fours_on]
    assert nine.shape == (1,7)
    nine = nine[0]

    # zero is the other one
    zero = zero_nine[~all_fours_on]
    assert zero.shape == (1,7)
    zero = zero[0]

    # these are two, three or five
    two_three_five = nums_by_segcount(5)
    assert two_three_five.shape == (3,7)

    # three is the one that shares all one's lit segments
    all_ones_on = np.all(two_three_five[:,one], axis=1)
    three = two_three_five[all_ones_on]
    two_five = two_three_five[~all_ones_on]
    assert three.shape == (1,7)
    assert two_five.shape == (2,7)
    three = three[0]

    # five is the one that shares five segments with six
    shared_with_six = np.sum(two_five[:,six], axis=1)
    five = two_five[shared_with_six == 5]
    assert five.shape == (1,7)
    five = five[0]

    # two shares four segments with six
    two = two_five[shared_with_six == 4]
    assert two.shape == (1,7)
    two = two[0]

    result = np.stack([zero, one, two, three, four, five, six, seven, eight, nine])
    assert result.shape == (10,7)
    result = np.apply_along_axis(row_to_num, 1, result)
    assert result.shape == (10,), result.shape

    return result


# deduce symbols for each row
ordered_symbols = np.stack([deduce_symbols(row) for row in unique_symbols])
assert ordered_symbols.ndim == 2
output_numbers = np.array([[row_to_num(r) for r in line] for line in output_numbers])

def lookup(syms, x):
    return int(np.where(syms==x)[0][0])

answer = sum(
    int("".join(str(lookup(syms, x)) for x in output_line))
    for syms, output_line in zip(ordered_symbols, output_numbers)
)
print(f"Part 2: {answer}")
