import time
import numpy as np
from scipy.ndimage import label
from scipy.signal import argrelmin


def timeit(f):
    def decorated(*args, **kwargs):
        start = time.time()
        r = f(*args, **kwargs)
        stop = time.time()
        print(f"Ran {f.__name__} in {(stop-start)*1000}ms")
        return r
    return decorated

# part 0: parsing

data = np.genfromtxt("input", delimiter=1, dtype=np.uint8)


# part 1: sum of smallest values
# this is solved in multiple ways lol

@timeit
def part1_method1():
    # shift input data along the four cardinal directions
    # pad with out-of-range value to make shapes match
    pad_value = 10
    shift_up = np.pad(data[1:,:], [(0,1), (0,0)], constant_values=pad_value)
    shift_down = np.pad(data[:-1,:], [(1,0), (0,0)], constant_values=pad_value)
    shift_left = np.pad(data[:,1:], [(0,0), (0,1)], constant_values=pad_value)
    shift_right = np.pad(data[:,:-1], [(0,0), (1,0)], constant_values=pad_value)

    # make bool matrix showing whether values are smaller than neighbors
    is_lowest = (data < shift_up) & (data < shift_down) & (data < shift_left) & (data < shift_right)

    # get answer
    lowest = data[is_lowest]
    answer = np.sum(lowest + 1)
    print(f"Part 1: {answer}")

part1_method1()


@timeit
def part1_method2():
    # surround input with 9s
    pdata = np.pad(data, (1,1), constant_values=9)

    # find minima along each axis
    ycs = argrelmin(pdata, axis=0)
    xcs = argrelmin(pdata, axis=1)

    # make bool arrays with minimas marked
    ys = np.zeros(pdata.shape, dtype=bool)
    xs = np.zeros(pdata.shape, dtype=bool)
    ys[ycs[0], ycs[1]] = True
    xs[xcs[0], xcs[1]] = True

    # 2d minima are the ones that satisfy both axes
    lowest = pdata[ys & xs]
    answer = np.sum(lowest + 1)

    print(f"Part 1: {answer}")

part1_method2()


# part 2: find basins
# label contiguous basin areas
labeled_basins, basin_count = label(data != 9)

# count non-zero labels to find basin areas
basin_areas = np.bincount(labeled_basins[labeled_basins != 0])

# answer is the product of the top three
answer = np.product(np.sort(basin_areas)[-3:])
print(f"Part 2: {answer}")


# part 1 - again
@timeit
def part1_method3():
    biggest_area = np.max(basin_areas)
    flatdata = data.flatten()
    default = np.tile(10, data.shape[0] * data.shape[1])

    # I really tried to avoid a list comprehension here
    # is there a numpy way to do this? tell me pls
    basin_values = np.array([
        # rows are flattened basin values
        np.where(
            (labeled_basins == (i + 1)).flatten(),
            flatdata,
            default
        )
        for i in range(basin_count)
    ])

    # local minima smallest value in a basin
    lowest = np.min(basin_values, axis=1)
    answer = np.sum(lowest + 1)
    print(f"Part 1: {answer}")

part1_method3()
