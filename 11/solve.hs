import System.IO  
import Control.Monad
import Data.Char
import Data.List as List
import Data.Sequence as Sq
import Data.Foldable

-- type Matrix a = [[a]]
type Matrix a = Seq (Seq a)

mxMap :: (a -> b) -> Matrix a -> Matrix b
mxMap f m =
  -- map a function element-wise over a matrix
  fmap (fmap f) m


flashing_coords :: Matrix Int -> [(Int, Int)]
flashing_coords m =
  -- give coords that have value above 9
  [(x,y) | y <- [0..(Prelude.length m) - 1],
           x <- [0..(Prelude.length $ index m 0) - 1],
           9 < (index (index m y) x)]


make_2seq :: [[a]] -> Seq (Seq a)
make_2seq m =
  fromList [fromList xs | xs <- m]


set_coord :: a -> Matrix a -> (Int, Int) -> Matrix a
set_coord v m (x, y) =
  -- m[x,y] = v
  let 
    newrow = update x v $ index m y
    newmatrix = update y newrow m
  in
    newmatrix


get_coord :: Matrix a -> (Int, Int) -> a
get_coord m (x, y) =
  -- m[x,y]
  index (index m y) x


increment_neighbors :: Matrix Int -> (Int, Int) -> Matrix Int
increment_neighbors m (x, y) =
  -- increment neighbords to given coord
  let
    w :: Int
    w = Sq.length $ index m 0

    h :: Int
    h = Sq.length m

    neighbors :: [(Int, Int)]
    neighbors = [(x-1, y-1), (x, y-1), (x+1, y-1),
                 (x-1,   y),           (x+1,   y),
                 (x-1, y+1), (x, y+1), (x+1, y+1)]

    incr_coord :: Matrix Int -> (Int, Int) -> Matrix Int
    incr_coord mx p =
      let 
        current = get_coord mx p
        new = if current == 0 then 0 else 1 + current
      in
      set_coord new mx p
  in
  foldl incr_coord m neighbors


any_flashers :: Matrix Int -> Bool
any_flashers m =
  -- true if any values in matrix are greater than 9
  (/=) Nothing $ findIndexL (\row -> ((/=) Nothing) $ findIndexL (\x -> 9 < x) row) m


innerStep :: (Matrix Bool, Matrix Int) -> (Matrix Bool, Matrix Int)
innerStep (has_flashed, m) =
  let 
    -- find all >9s
    currently_flashing = flashing_coords m

    -- fold increment their neighbors
    incremented :: Matrix Int
    incremented = foldl increment_neighbors m currently_flashing

    -- set >9s to 0
    zeroed :: Matrix Int
    zeroed = foldl (set_coord 0) incremented currently_flashing

    -- add to has_flashed
    new_has_flashed :: Matrix Bool
    new_has_flashed = foldl (set_coord True) has_flashed currently_flashing

    result = ( new_has_flashed , zeroed)

  in
  -- recurse if there are any new >9s
    if any_flashers zeroed then
      innerStep result
    else
      result


outerStep :: Int -> [Matrix Bool] -> Matrix Int -> ([Matrix Bool], Matrix Int)
outerStep roundsLeft all_flashes m =
  if roundsLeft == 0 then
    (all_flashes, m)
  else 
    let
      -- increment all with 1
      incremented :: Matrix Int
      incremented = mxMap ((+) 1) m

      w = Sq.length $ index m 0
      h = Sq.length m

      falseys :: Matrix Bool
      falseys = Sq.replicate h $ Sq.replicate w False

      -- do innerStep until there are no more flashes
      flashes :: Matrix Bool
      new_m :: Matrix Int
      (flashes, new_m) = innerStep (falseys, incremented)
    in
    -- recurse
    outerStep
      (roundsLeft - 1)
      (flashes:all_flashes)
      new_m


pretty :: Matrix Int -> String
pretty m =
  intercalate "\n" $ fmap (\row -> intercalate ", " $ fmap show (toList row)) (toList m)


pp m =
  putStrLn $ pretty m


count_trues :: Matrix Bool -> Int
count_trues m =
   sum $ fmap (foldl (\count b -> if b then count+1 else count) 0) m


all_true :: Matrix Bool -> Bool
all_true m =
   foldl (&&) True (fmap (foldl (&&) True) m)


main = do
  -- read input
  filetxt <- readFile "input"
  let initial = fmap (fmap digitToInt) $ lines filetxt :: [[Int]]
  let m = fromList (map fromList initial) :: Matrix Int

  let (flashes, final_m) = outerStep 500 [] m
  putStr "Part 1: "
  print $ sum (map count_trues (List.take 100 $ List.reverse flashes))
  putStrLn ""

  -- day 2: find first day with all true
  let answer = findIndexL all_true $ Sq.fromList (List.reverse flashes)
  putStr "Part 2:"
  print $ fmap ((+) 1) answer
