import numpy as np

# part 0: parsing

data = np.genfromtxt("input", delimiter=1, dtype=np.uint8)


# part 1: simulate flashing

def neighbors(xy):
    """ return bool matrix marking neighbors of coords `xy` """
    assert xy.shape == (2,), xy.shape

    # add offsets to input coord
    offsets = np.array([
        [-1,-1], [0,-1], [1,-1],
        [-1, 0],         [1, 0],
        [-1, 1], [0, 1], [1, 1],
    ])
    offxys = xy + offsets
    assert offxys.shape == (8,2), offxys.shape

    # make map of neighbors to `xy`
    r = np.zeros(data.shape, dtype=bool)
    inxys = offxys[
        # keep only the in-bound coords
        (0 <= offxys[:,0]) & (offxys[:,0] < data.shape[0]) &
        (0 <= offxys[:,1]) & (offxys[:,1] < data.shape[1])
    ]
    r[tuple(inxys.transpose())] = True
    assert r.shape == data.shape
    return r


# run simulation for `rounds` steps
rounds = 300
all_flashes = np.zeros((*data.shape, rounds), dtype=int)
for roundnum in range(rounds):
    # a step
    data += 1

    flashed_this_round = np.zeros(data.shape, dtype=bool)

    # find octopi value 10 or greater 
    while np.any(flashing := (9 < data)):
        # keep record of which to reset when all is said and done
        flashed_this_round |= flashing

        # indexes of flashing octopi
        flashing_idxs = np.stack(np.nonzero(flashing))
        assert flashing_idxs.shape[0] == 2

        # neighbors of flashing octopi
        flash_affected = np.apply_along_axis(neighbors, 0, flashing_idxs)
        assert flash_affected.shape == (*data.shape, flashing_idxs.shape[1])

        # the amount to increment each cell with, the flash effect
        flash_addition = np.sum(flash_affected, axis=2, dtype=np.uint8)
        assert flash_addition.shape == data.shape

        # do flash effect
        data += flash_addition
        data[tuple(flashing_idxs)] = 0

        # repeat if there are any new ones to flash

    data[flashed_this_round] = 0
    all_flashes[:,:,roundnum] = flashed_this_round

    if roundnum > 100 and np.all(flashed_this_round):
        # synchronized!
        break


# find the sum of flashes in the first 100 rounds
answer = np.sum(all_flashes[:,:,:100])
print(f"Part 1: {answer}")


# part 2 - find earliest round where all octopi flash
answer = 1 + np.argmax(np.all(all_flashes.reshape((-1, all_flashes.shape[2])), axis=0))
print(f"Part 2: {answer}")
