import numpy as np

# day 3

# part 0: parsing
bits = np.genfromtxt('input', delimiter=1, dtype=np.uint8)

# part 1
def most_and_least_common(column):
    count = np.bincount(column)
    return count.argmax(), count.argmin()


def bitarray_to_int(xs):
    return int("".join(str(b) for b in xs), 2)


most_common, least_common = np.apply_along_axis(most_and_least_common, 0, bits)
gamma = bitarray_to_int(most_common)
epsilon = bitarray_to_int(least_common)
answer = gamma * epsilon
print(f"Part 1: {answer}") # 3549854


# part 2
def least_common_bit(column):
    count = np.bincount(column)
    same = np.all(count == count[0])
    return count.argmin() if not same else 0


def most_common_bit(column):
    count = np.bincount(column)
    same = np.all(count == count[0])
    return count.argmax() if not same else 1


def find_by_elimination(criteria, a=bits, i=0):
    a = a[a[:,i] == criteria(a[:,i])]
    if a.shape[0] == 1:
        return a[0]
    return find_by_elimination(criteria, a, i+1)


o2 = bitarray_to_int(find_by_elimination(most_common_bit))
co2 = bitarray_to_int(find_by_elimination(least_common_bit))
result = o2 * co2
print(f"Part 2: {result}")
