import numpy as np
from bokeh.plotting import figure, show, save
from functools import partial
from itertools import product

# part 0: parsing
with open("input") as f:
    rawtxt = f.read()[len("target area: "):].strip()

target_xs = rawtxt[2:].split(",")[0].split("..")
xmin, xmax = int(target_xs[0]), int(target_xs[1])
target_width = xmax - xmin

target_ys = rawtxt.split(", y=")[1].split("..")
ymin, ymax = int(target_ys[0]), int(target_ys[1])
target_height = ymax - ymin


# part 1: find launch with highest y pos

def decelerating(init_v, t):
    return t * (2*init_v - t + 1)/2 


def altitude(init_y, t):
    return decelerating(init_y, t)


def distance(init_x, t):
    return decelerating(init_x, min(t, init_x))


def make_data(ix, iy, truncate=False):
    # make data points, calculate a bit too much
    ts = np.arange(0, 500)
    xs = np.vectorize(partial(distance, ix))(ts)
    ys = np.vectorize(partial(altitude, iy))(ts)

    # truncate to where ys < ymin
    if truncate:
        last_idx = np.argmax(ys < ymin)
        xs = xs[:last_idx]
        ys = ys[:last_idx]
    return np.vstack([xs, ys])


# shitty brute solve
xrange, yrange = range(200), range(-200, 200)
inits = np.array(list(product(xrange, yrange)))
scores = [
    ((ix, iy), np.max(xys[1]))
    for ix, iy in inits
    if np.any(
        (xmin <= (xys := make_data(ix, iy))[0]) & (xys[0] <= xmax) &
        (ymin <= xys[1]) & (xys[1] <= ymax)
    )
]
init_v = max(scores, key=lambda x: x[1])


# make plot
xs, ys = make_data(*init_v[0], truncate=True)
plot = figure()

# launch point
plot.circle([0], [0])

# draw target rectangle
plot.rect(
    [xmin + target_width/2],
    [ymin + target_height/2],
    target_width,
    target_height
)

# plot trajectory
plot.line(xs, ys, color="red")
plot.circle(xs, ys, size=10, color="red")

save(plot)
