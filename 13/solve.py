import numpy as np

# part 0: parsing

inputfile = "input"
coords = np.genfromtxt(
    inputfile,
    delimiter=",",
    comments="fold",
    dtype=int
)

folds_raw = np.genfromtxt(
    inputfile,
    delimiter="=",
    skip_header=len(coords),
    dtype=str
)


def _to_num(s: str) -> int:
    if s.endswith("x"):
        return 1
    elif s.endswith("y"):
        return 0
    elif s.isdigit():
        return int(s)
    assert not "FUCK"

to_num = np.vectorize(_to_num)
folds = to_num(folds_raw)


# part 1: a fold

# folds are always halfway (dropping one col/row)
# find paper dimensions by looking at first fold in each direction
first_xfold = folds[folds[:,0] == 1][0][1]
first_yfold = folds[folds[:,0] == 0][0][1]
w = first_xfold * 2 + 1
h = first_yfold * 2 + 1

# make initial paper
paper = np.zeros((h, w), dtype=bool)
paper[coords[:,1], coords[:,0]] = True

def fold_along_axis(arr, axis, offset):
    if axis == 0:
        return arr[:offset] | arr[offset+1:][::-1]
    if axis == 1:
        return arr[:,:offset] | arr[:,offset+1:][:,::-1]

paper = fold_along_axis(paper, *folds[0])
part1_answer = np.sum(paper)
print(f"Part 1: {part1_answer}")


# part 2: do all folds
for fold in folds[1:]:
    paper = fold_along_axis(paper, *fold)

np.set_printoptions(formatter={"bool": ".#".__getitem__}, linewidth=99)
print("Part 2:")
print(paper)
