Solutions to [Advent of Code 2021](https://adventofcode.com/2021/)

This year I'll try to leverage numpy (and friends: pandas, scipy, etc) as much as possible.
